<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/{postCode}', function($postCode) {
    $postCode25 = PostcodeApi::create('PostcodeApiNu')->find($postCode);

    return Response::json($postCode25->toArray(), 200, [], JSON_PRETTY_PRINT);
});

@extends('layouts.app')

@section('title')
	Bewerk {{ $user->name }}
@endsection

@section('content')
{!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
<div class="form-group">
	<div class="col-sm-6">
		{!! Form::label('name', 'Naam*', ['class' => 'control-label']) !!}
		{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'De naam hier']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('postcode', 'Postcode hier', ['class' => 'control-label']) !!}
		{!! Form::text('postcode', null, ['class' => 'form-control', 'placeholder' => 'Postcode']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('huisnummer', 'Huisnummer hier', ['class' => 'control-label']) !!}
		{!! Form::text('huisnummer', null, ['class' => 'form-control', 'placeholder' => 'Huisnummer']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('straatnaam', 'Straatnaam', ['class' => 'control-label']) !!}
		{!! Form::text('straatnaam', null, ['class' => 'form-control', 'placeholder' => 'Straatnaam hier']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('woonplaats', 'Woonplaats', ['class' => 'control-label']) !!}
		{!! Form::text('woonplaats', null, ['class' => 'form-control', 'placeholder' => 'Woonplaats hier']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
		{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Het e-mailadres hier']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('role_id', 'Rol', ['class' => 'control-label']) !!}
		{!! Form::select('role_id', $roles, null, ['class' => 'form-control', 'placeholder' => 'Maak een keuze uit de lijst']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('location_id', 'Locatie', ['class' => 'control-label']) !!}
		{!! Form::select('location_id', $locations, null, ['class' => 'form-control', 'placeholder' => 'Maak een keuze uit de lijst']) !!}
	</div>
	</div>
	<div class="form-group">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-primary">
				Opslaan
			</button>
		</div>
	</div>
{!! Form::close() !!}

@endsection
@section('scripts')
<script type="text/javascript">
$("#postcode").on('change', function() {
	var postcode = $(this).val();
	if (postcode.length > 1 && postcode.length < 14) {
		console.log(postcode);
		postcodeZoeken(postcode);
	}
});
function postcodeZoeken() {
  var postcode = document.getElementById('postcode').value;
  var number = document.getElementById('huisnummer').value;
  console.log(postcode);
  console.log(number);
  $.ajax({
    url:"https://postcode-api.apiwise.nl/v2/addresses/?postcode=" + postcode,
    dataType: "json",
    type: "GET",
    headers: {
      "X-Api-Key": "SvFN5xK7cN4tZhrmf8b3Y7eY7ipsRGRt1BAytIAV"
    },
    success: function(data) {
      console.log(data);
      for (i=0; i<data._embedded.addresses.length; i++) {
        document.getElementById('straatnaam').value = data._embedded.addresses[i].street;
        document.getElementById('woonplaats').value = data._embedded.addresses[i].city.label;
      }
    }
  });
}

//document.getElementById('postcode').addEventListener('change', postcodeZoeken, false);
</script>
@endsection
